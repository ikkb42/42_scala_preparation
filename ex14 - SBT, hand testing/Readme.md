### ex14 ###
SBT + Ammonite = Love

### Ce que vous allez faire ###

* Interagir avec votre code source, grace a SBT et Ammonite

### Ce que vous devez faire ###

* Reutiliser les sources de votre `ex13`
* Ajouter la lib necessaire (cf la doc d'Ammonite (cf README.md a la source du repo))
* Lancer ammonite dans SBT
* Ecrire le code necessaire (dans ammonite) pour appeler la fonction factorielle (du code source) de maniere naturelle (exemple: `5!` au lieu de `fac(5)`)
* M'envoyer un screenshot de votre travail -> grosso modo, ca doit ressembler a mon screenshot en fait

### Consignes ###

* Faites appel a ce qu'on a appris, c'est pas complique

### Commentaire de l'auteur ###

```
Ammonite est vraiment cool
Dans ce cas precis, il est ultra pratique de pouvoir tester, et debuger son code, en direct dans sa REPL scala, dans la REPL sbt (`#ReplCeption`) !
Ca permet de ne pas avoir a ajouter des fichiers pour faire du micro testing de features. On peut faire tout ca en direct, sans se compliquer la vie.
```

![screen](http://oi63.tinypic.com/2d77gw.jpg)
