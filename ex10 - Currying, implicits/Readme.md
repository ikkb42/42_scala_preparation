### ex10 ###
Le currying
Les `implicit`

### Ce que vous savez faire ###

* utiliser des lambdas (fonctions anonymes), avec diffrerentes syntaxes
* declarer et utiliser des fonctions et des methodes, avec differentses syntaxes
* appeler des fonctions en parametre (mais avec une syntaxe d'appel pas tres concise)

### Ce que vous allez faire ###

* Decouvrir la curryfication en Scala (parce que c'est mieux, pour beaucoup de raisons esthetiques, semantiques, et fonctionnelles)
* Decouvrir ca avec d'autres approches fonctionnelles (je vous invite a lire ce qui touche au currying en OCaml, Haskell etc.. Wikipedia ne dit pas trop de conneries la-dessus)
* Manipuler les parametres implicites
* Manipuler les objets implicites (on se concentrera sur les classes implicites)

```
Vous pouvez declarer (et "prototyper") vos fonctions et methodes comme bon vous semble
```

### Ce que vous devez faire ###

* Creer une fonction `add` qui ajouteront 2 Int curried. Son type: `add: Int => Int => Int`
* Re-faire `applyDiff` de l'ex09, mais au lieu de lui declarer 2 parametres, vous allez les curry. Vous me ferez differents exemples d'appel de cette fonction (vous verrez, vous ne serez plus contraints de faire des appels moches, la syntaxe concise de Scala est de retour)
* Me faire un petit script qui fera printer "bonjour" a cette fonction `def hello(implicit s:String) = println(s)` -> Vous n'avez evidemment pas le droit de l'utiliser ainsi: `hello("bonjour")`
* Vous creerez la methode `printself` qui s'appliquera aux objets de type `String` qui devra faire un simple println de la String pour laquelle la methode est appellee (`"mdr".printself` devra faire un println de "mdr")
* Creer la methode `printit` qui fera comme `printself` mais sur n'importe quel type d'objet

### Cas pratiques ###

* Re-implementez `pMin` et `pMax` (ex05), mais en tant que methodes s'appliquant a `List[Int]` (parce que je suis sympa), servez vous des `implicit parameters`, gerez monadiquement les erreurs de collection vide
* Implementez les methodes `pForeach` et `pMap`, s'appliquant a des `List` de n'importe quel type. Attention, elles ne doivent pas faire appel aux `foreach` et `map` deja existants, je vous laisse reflechir, c'est pas difficile, et ca tient en une mini-ligne.

### Consignes ###

* Lisez plus, toujours plus (le currying, les implicit c'est pas dur, et c'est foutrement pratique)
