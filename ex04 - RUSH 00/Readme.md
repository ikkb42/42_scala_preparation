# Rush00 #

Petit test de vos acquis

### Ce que vous avez vu ###

* L'immutabilite
* Les stuctures de donnees (avec plein de formes de collections)
* Les methodes d'iteration et de transformation de stuctures de donnees
* Les fonctions anonymes (lambdas expressions)
* L'objet en Scala
* Le pattern matching
* Les manipulations IO (enfin, le println, le fromFile... c'est bien assez)
* Les monades communes (en surface, mais c'est suffisant)
* Le code concis, la lib standard qui fait de cafe, les sucres syntaxiques de Scala

### Ce que vous devez faire ###

* Un `cat` mais en plus joli
```
Le cat est sympa, mais des qu'on code avec des TABS,
l'output est horrible !

Je vous ai facilite la tache, le template de code est pret ('prettyPrint.scala')
Vous n'aurez qu'a implementer une seule fonction

L'objectif ici sera de print le contenu du ficher 'src.scala',
dans lequel j'ai mis du code moche, et plein de TABS :)

Il vous faudra donc remplacer ce bordel de TABS pour avoir un affichage propre
Comme si l'on ouvrait le fichier dans un editeur intelligent

En l'occurence, les TABS ont une taille max de 4 caracteres

Vous avez le droit de reduire cette taille ou de mettre des couleurs,
mais autant aller a l'essentiel, je veux au moins que l'output ressemble au mien
```

* Implenter la fonction `prettyPrint(s:String):Unit`
```
Vous devez tout faire a l'interieur de cette fonction.

Vous pouvez creer creer des fonctions supplementaires (je l'ai fait),
mais elle devront etre implementees a l'interieur de 'prettyPrint'.

Essayez de faire coder moins de 10 lignes dans le body de votre fonction.
J'ai fait tenir ca en 3 lignes, mais ca peut tenir en 1 seule (mais moche).
```

### Recommandations ###

```
Ne vous prenez pas trop la tete,
faites vous plaisir (je ne le dirai jamais assez).

Il suffit de reflechir posement, et d'aller a l'essentiel
Faites du Scala aussi idiomatiques que possible -> concis, pas moche, immutable
(utilisez certaines des notions que vous avez vues)

Tiens je le redis, FAITES PAS DU CODE MOCHE svp
Vous pouvez faire plus de 80 colonnes pas lignes (mais abusez pas trop)
```

### OUTPUT etc ###

![screenshot](http://oi63.tinypic.com/sb3rc4.jpg)
