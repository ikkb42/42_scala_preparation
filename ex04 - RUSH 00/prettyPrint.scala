import io.Source.fromFile
import java.io.{FileNotFoundException => ENOF}

trait Pretty {
  def prettyPrint(s:String) = {
    ???
  }
}

object Main extends Pretty {
  def usage = println("Give a valid file as an argument")

  def main(av:Array[String]):Unit = av.length match {
    case 1 => try {
        prettyPrint( fromFile(av.head).mkString )
      } catch {
        case e:ENOF      => usage
        case x:Throwable => println("Unexpected exception: " + x)
      }
    case _ => usage
  }
}
