object Main {
	class Demo {
		private var _attr: String		= _
		lazy val attr					= "public lazy"

		val sweg						= "public"

		private def setAttr(s:String)	= {
			_attr = s
		}

		setAttr("private")

		override def toString			=	"_attr : " + _attr + "\n" +
											"attr  : " +  attr + "\n" +
											"sweg  : " +  sweg
	}

	def main(av:Array[String]) = {
		val demo = new Demo
		println( demo.toString )
	}
}
