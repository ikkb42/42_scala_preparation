### ex08 ###
Toujours plus d'objet

### Ce que vous avez vu precedemment ###

* l'heritage (de maniere un peu plus etendue)
* les objets 'communs': class, object, trait
* un objet intelligent: companion object -> vu en avez vu les avantages (`import` du contenu d'une class grace a l'heritage, possibilite un companion en tant que pattern...), mais peut etre aussi les inconvenients (plus de code a taper, reimplementation, ajout de code dans le companion...)

Aujourd'hui, vous allez mettre de cote ces notions, et decouvrir les `case class`
```
Lisez, documentez vous a fond, les case class, c'est comme foldLeft: une putain de fonctionnalite !!!
Renseignez vous sur:
- les utilisations qu'on peut en faire
- l'avantage NON NEGLIGEABLE quant a la taille du code
- les methodes apply / unapply
```

Une fois encore vous allez faire vos tests directement dans votre REPL (`ammonite` ou `scala`)

### Nota Bene ###

* Il y a plein de tutos sur internet #entrainezVous
* Testez les... pattern matching (si vous avez approfondis `ex07`, vous deduirez en vous entrainant, que les case class peuvent remplacer proprement les companion object et les class)
* Les case class creent des types a part entiere, et peuvent, entre autre, permettre de creer des pseudo-tuples propres

### Ce que vous devez faire ###
On va creer un objet `Caesar` -> chiffrant une String donnee avec un `rot(n)`
```
Soyez bien attentifs, analysez le screenshot
il y a forcement des questions que vous allez vouloir me poser, qui trouvent leur reponse en LISANT ATTENTIVEMENT LE SCREENSHOT
Donc je vous demande moult attention
(moi-meme je voudrais poser des questions betes a votre place, mais faut pas quand les reponses sont la !)
```
```
Creez une case class Caesar. Observez bien tout le screenshot ! Pour deviner son prototypage et sa declaration
Elle devra contenir les attributs et methodes suivants:
- rot      : Int       //variable: renvoie le nombre de rotations alphabetiques pour chiffrer les caracteres du texte
- isCipher : Boolean   //methode:  verifie que le texte est bien chiffre (donc, different du texte original)
- isPlain  : Boolean   //methode:  verifie que le texte est bien dechiffre
- decrypt  : Caesar    //methode:  dechiffre le texte, et je vous laisse deviner le retour
- encrypt  : Caesar    //methode:  chiffre le texte, et je vous laisse deviner le retour
- toString : String    //mdr

Vous pouvez (et devez) ajouter des methodes (dont une en particulier) et attributs (meme des var, mais seulement ce qui est utile)
Tous les attibuts et methodes (sauf la fameuse) que vous ajouterez, devront etre 'private' !

Pour la fameuse methode, si vous avez bien fait vos recherches, et que vous regardez attentivement le screenshot,
vous saurez comment la nommer et la declarer (sans avoir a me demander)
```
Bonne chance, ca devrait aller tranquillement :)

![car screenshot](http://oi65.tinypic.com/2v1k878.jpg)
