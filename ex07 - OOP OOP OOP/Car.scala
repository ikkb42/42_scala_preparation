trait Shine {
  var shiny      = false
  def shineState = if (shiny == true) "shines" else "doesn't shine"
}

class Colors {
  val red    = "Red"
  val blue   = "Blue"
  val yellow = "Yellow"
}

object Brands {
  val brands = "Ford" :: "Nissan" :: "Audi" :: Nil
}
