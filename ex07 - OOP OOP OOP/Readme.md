### ex07 ###
L'objet l'objet l'objet :)

### Ce que vous avez deja vu ###

* traits (sortes de classes abstraites, dans lesquelles les methodes peuvent etre implementees)
* class (ben vous savez ce que c'est quand meme)
* object (une sorte de singleton, mais sans la syntaxe chiante des singletons)
* l'heritage (de maniere basique)

Aujourd'hui, vous allez approfondir ces notions, et decouvrir les `companion object`
```
l'utilisation de tous ces objets est a bien connaitre (comment on declare, quid des params etc..)
```

De plus vous allez faire vos tests directement dans votre REPL (`ammonite` ou `scala`), vous verrez dans le screenshot comment vous y prendre.

Observez attentivement le screenshot et les appels que je fais. Ils vous montreront comment tout fonctionne (je ne repondrai pas aux questions qui trouvent leur reponses dans les recherches que vous devez faire avant de faire l'exo)

### Ce que vous devez faire ###
```
Creez une class Car. Observez bien tout le screenshot ! Pour deviner son prototypage et sa declaration
Elle devra contenir 4 methodes, dont je vous donne le nom et le type, pour vous faire gagner une minute de reflexion:
- checkColor: String => Boolean  //privee, qui determine si une couleur est valide (si elle appartient bien a la class Colors)
- paint     : String => Unit     //si la couleur est valide (checkColor), donnant une nouvelle couleur a la voiture
- mustShine : Unit               //fait briller la voiture (sauf si elle brille deja)
- toString  : String             //mdr

Vous n'avez pas le droit de modifier les objets figurants dans le template 'Car.scala'
(je ne veux pas voir de nouveaux attributs qui les composent, ou alors de declaration modifiee. ils sont la, on y touche po)

Vous pouvez cependant creer de nouveaux objets (class, object, trait), pour permettre a votre code de compiler.
Si vous avez bien fait vos recherches, cette precedente declaration vous semblera evidente.

Pas besoin de gestion d'erreur monadique (sauf si vous y tenez vraiment, mais pas necessaire, ca ne fera qu'alourdir votre code)
```
Bonne chance, ca devrait aller tranquillement :)
```
NB: Faites de l'objet idiomatique Scala ! J'insiste, Scala c'est pragmatique, on evite les ceremonies inutiles
```

![car screenshot](http://oi66.tinypic.com/24g7wg0.jpg)
