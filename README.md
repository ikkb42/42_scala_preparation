# README #

A Propos

### Quoi que c'est ? ###

* Un repo, avec des exercices
* Un repo pour vous former sur Scala
* Un repo pour vous introduire au data engineering
* Un repo pour vous fournir des exercices de preparation a integrer des entreprises qui savent vraiment faire du Scala
* Un repo pour les gouverner tous
* Un repo pour les trouver
* Un repo pour les amener tous et dans les ténèbres les lier

### Quoi qu'il faut d'abord faire ? ###

* Installer Scala
* Installer [Ammonite-Repl](http://www.lihaoyi.com/Ammonite/#GettingStarted)

### Quoi qu'il faut faire pour chaque exercice ? ###

* BIEN LIRE les consignes
* BIEN RELIRE les consignes
* M'envoyer en MP Slack, avec `un joli formatage` vos codes permettant de reussir les exos

### Comment qu'on fait pour reussir les exos ? ###

* [Scala By Example (votre Bible, votre journal intime)](http://www.scala-lang.org/docu/files/ScalaByExample.pdf)
* [La Scaladoc (pour approfondir vos decouvertes)](http://www.scala-lang.org/api/current/#package)
* Internet: git, stackoverflow...
* APPROFONDIR CHAQUE DECOUVERTE, etre studieux

### Comment qu'on fait quand on a fini ? ###

* On peut me faire un bisou, m'offrir une bière
* Il est bien de s'entrainer aussi avec d'autres exos, plus officiels: https://www.scala-exercises.org/std_lib

### Est-ce que rbobillo est un fdp veritable ? ###

* Oui

### Commentaires de l'auteur ###

* Mdr
* lol
* Vous faites du Scala, faites tout pour vous simplifier la vie
* Scala est ma passion, j'aime Scala
* Le fonctionnel melange au pur objet, c'est extra
* Bisous
