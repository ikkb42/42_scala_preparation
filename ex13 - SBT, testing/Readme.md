### ex13 ###
SBT, et scalatest

### Ce que vous allez faire ###

* Utiliser le framework de test `specs2` avec `sbt`
* Grosso modo, un peu de Unit Testing, a la cool
* Tester et ameliorer le programme de l'ex12 part3

### Ce que vous devez faire ###

* Tester la fonction factorielle avec des valeurs positives: `1, 8, 15, 1000`
* La tester avec une valeur nulle: `0`
* La tester avec des valeurs negatives: `-1, -10` //ce test doit fail
* Ameliorer la fonction factorielle //elle doit desormais renvoyer soit une String `"Error: n < 0"`, soit un BigInt (le resultat)... -> il y existe une super Monade pour ca (et ce n'est pas `Option`)
* Updater vos tests afin de les faire matcher au nouveau type de la fonction factorielle

### Consignes ###

* Familiarisez vous bien avec `specs2`
* Faites des test precis et joliment detailles (a votre guise, laissez libre cours a votre inspiration)
