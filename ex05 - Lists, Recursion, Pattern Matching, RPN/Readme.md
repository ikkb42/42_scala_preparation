### ex05 ###

Vous allez devoir vous familiariser differentes (2) methodes de declarer et d'utiliser des `List`.

Today, vous allez faire du `recursif`, du `pattern matching`, ainsi que decouvrir le principe de parametres par defaut

Exemple d'utilisation de List (parce que je suis sympa):
```
def isEmpty(l:List[Int]) = l match {
  case h :: t => false
  case Nil    => true
}
```
### Ce que vous devez faire ###

* Creer un `trait PersoInt` qui deva contenir les fonctions suivantes
*   pLen  //renvoie la taille de la List[Int] recue en param
*   pRev  //renvoie la List[Int] en ordre inverse
*   pSum  //renvoie la somme des elements de la List[Int]
*   pMin  //renvoie le plus petit element de la List[Int]
*   pMax  //renvoie le plus grand element de la List[Int]
*   pHead //renvoie le premier element de la List[Int]
*   pLast //renvoie le dernier element de la List[Int]
*   pTail //renvoie la List[Int] sans le premier element

A vous de les creer, en reflechissant bien pour la facon de les prototyper
Elles seront ainsi appellees
```
val list = List(5,1,8,2) //par exemple

val len  = pLen(list)  // 4
val rev  = pRev(list)  // List(2,8,1,5)
val sum  = pSum(list)  // 16
val min  = pMin(list)  // 1
val max  = pMax(list)  // 8
val head = pHead(list) // 5
val last = pLast(list) // 2
val tail = pTail(list) // List(1,8,2)
```

---

### Exercice d'application ###
Il va s'agir de faire de la manipulation de List, pour parser une String.

En l'occurence, vous allez devoir faire un petit parseur d'operations en notations post-fixee !

Ca va vous permettre d'explorer la puissance du pattern matching et du recursif en Scala -> faire du code propre et concis qui fait plein de trucs

### Ce que vous devez faire ###
Un parseur de RPN, au calme
Implementer la fonction `parse: String => Int`, qui, comme l'indique son prototype, prend une `String` et renvoie un `Int`. Ainsi que toute autre fonction, vous permettant d'atteindre votre objectif, tant que ce que vous faites, reste dans une approche fonctionnelle !
```
//les composants de la String d'operation, seront separe par des espaces

/* les appels seront ainsi faits:
var res = 0

res = parse( "40 2 +" )                // 42
res = parse( "2 40 2 + 2 / *" )        // 42
res = parse( "1 2 + 4 * 3 + 6 + 2 *" ) // 42
res = parse( "2 1 2 + 4 * 3 + 6 + *" ) // 42
*/
```
