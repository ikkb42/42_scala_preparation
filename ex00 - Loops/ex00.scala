// 1
for (a <- 1 to 42) {
  println("42");
}

// 2
for {
    i <- 1 to 42
} println("42")

// 3
List.fill(42)("42").foreach(println)
