Ecrivez moi 3 scripts scala, qui permettent d'afficher 42 fois la String "42"

La fonction d'affichage à utiliser sera println()

Le but de l'exercice est de découvrir les différentes facons de faire des 'boucles' en Scala
(approche iterative, idiomatique scala, fonctionnelle (avec une methode))

Ces boucles ne doivent rien renvoyer, mais uniquement faire 42 `println("42")`
