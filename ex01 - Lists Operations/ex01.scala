val srcList = List(1,2,3,4,5,6)

val newList: List[Int] = srcList.map(_ * 2)
  // une copie de srcList
  // dans laquelle chaque element aura ete multiplie par 2
  // -> vous pouvez utiliser for (cf "for comprehensions"))
  // -> je vous conseille d'utiliser une methode bien connue
  //    (une sorte de foreach, mais qui ne renvoie pas rien)

val sumOfNewList: Int = newList.sum
  // la somme de la nouvelle liste (vous avez le droit a tout, tant que c'est simple)

val about: String = s"Sum of ${newList.mkString("[", ",", "]")} = $sumOfNewList"
  // ca doit nous renvoyer cette String:
  // "Sum of [2,4,6,8,10,12] = 42"
  // Parole, le code a faire pour obtenir ca, tient sur une ligne assez courte
  //  (une seule methode appelee)
