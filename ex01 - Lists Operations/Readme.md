```
/////////////////////////////////////////////////////////////////////////////
// Vous devez me renvoyer tout ca, sans les commentaires :)
//
// A la place des commentaires, vous ecrirez le code vous permettant
//  d'obtenir le resultat attendu
//
// Allez au plus simple, demandez a Google et a ScalaByExample,
// chaque code peut tenir sur une petite ligne :)
/////////////////////////////////////////////////////////////////////////////
```
```
val srcList = List(1,2,3,4,5,6)

val newList: List[Int] =
  // une copie de srcList
  // dans laquelle chaque element aura ete multiplie par 2
  // -> vous pouvez utiliser for (cf "for comprehensions"))
  // -> je vous conseille d'utiliser une methode bien connue
  //    (une sorte de foreach, mais qui ne renvoie pas rien)

val sumOfNewList: Int =
  // la somme de la nouvelle liste (vous avez le droit a tout, tant que c'est simple)

val about: String =
  // ca doit nous renvoyer cette String:
  // "Sum of [2,4,6,8,10,12] = 42"
  // Parole, le code a faire pour obtenir ca, tient sur une ligne assez courte
  //  (une seule methode appelee)
```
