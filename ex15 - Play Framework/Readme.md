"""
### ex15 ###
Play + Scala

### Objectif ###
 - apprendre a utiliser Scala dans un IDE
 - apprendre a utiliser le framework Play avec Scala
 - s'introduire au minimum vital du machine learning

### Ce que vous devez faire ###
 - Part 1:
   -> Installer IntelliJ Ultimate
   -> Y ajouter les plugin Scala et SBT (sauf s'ils y sont par defaut)
   -> Y ajouter (si vous voulez) le plugin IdeaVim (ca permet de simuler Vim dans votre IDE, moi j'adore)
   -> Creer un nouveau projet au doux nom de `regression`, de type Play (laisser faire l'installation, qui sera assez longue, vous devriez avoir un template créé tout seul)
   ———> NB: Si jamais vous en chiez pour creer le projet,
     - creez le dans le terminal avec Activator (https://www.playframework.com/documentation/2.5.x/NewApplication)
     - importez le dans IntelliJ, en tant que projet SBT

 - Part 2:
   -> Faire le sujet de Baptiste (ft_linear_regression) en Scala
   -> Tant qu'a faire, commencez par le faire bien (avec une console, un rendu graphique (avec la lib `breeze`))

 - Part 3
   -> Faire fonctionner vos algos de regression lineaire dans votre application Play

### Consignes pour votre app Play ###
 - Familiarisez vous bien avec Play, lisez bien la doc, essayez des tutos
 - Si vous ne connaissez pas la notion de MVC -> go wikipedia
 - Creer le endpoint "linearregression", de type GET
 - Incorporez vos algos dans le code source de l'application (reflechissez bien a la place et au fonctionnement de vos fichiers source)

### Fonctionnement ###
 -> Minimaliste
 -> Requete par GET (a la place de la console, dans le sujet original), avec ce format: "http://localhost/linearregression?mileage="

 Par exemple:
    http://localhost/linearregression?mileage=42000
 affichera une page blanche, avec pour unique texte, la prédiction en euros (en l'occurence "This car worth 7566 euros")
"""
