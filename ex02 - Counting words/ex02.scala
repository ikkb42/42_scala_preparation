import scala.io.Source
import scala.collection.immutable.ListMap

val words = Source.fromFile("pcs.txt").getLines.mkString(" ").split(" ")
val wordsCount = words.groupBy(identity).mapValues(_.size).filter(_._1.nonEmpty)
val sortedWords = ListMap(wordsCount.toSeq.sortWith(_._2 > _._2):_*)
val mostRecurrentWords = sortedWords.take(5)

mostRecurrentWords.map(x => s"${x._1}: ${x._2}") foreach println
