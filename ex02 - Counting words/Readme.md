3e exercice, ca se complique :)
Maintenant que vous avez vu `foreach`, `map` ou les `for comprehensions`, ainsi que l’utilisation des `lambas` (fonctions anonymes), il est temps d’appliquer ca a un exemple concret

```
Il etait un petit homme
Pirouette cacahuete
Il etait un petit homme
Qui avait une drole de maison
Qui avait une drole de maison

La maison est en carton
Pirouette cacahuete
La maison est en carton
Les escaliers sont en papier
Les escaliers sont en papier

Si vous voulez y monter
Pirouette cacahuete
Si vous voulez y monter
Vous vous casserez le bout du nez
Vous vous casserez le bout du nez

Le facteur y est monte
Il s'est casse le bout du nez
On lui a raccommode
Avec du joli fil dore
Avec du joli fil dore

Mon histoire est terminee
Pirouette cacahuete
Mon histoire est terminee
Messieurs mesdames applaudissez
Messieurs mesdames applaudissez
```
Copiez ce texte dans un fichier que vous nommerez `pcs.txt`

Vous devrez me faire un script Scala (ou un beau programme, si ca vous tente, mais un script suffira).
```
Ce script doit lire le fichier "pcs.txt", et afficher quels sont les 5 mots les plus récurrents dans le texte.
```
Votre script devra printer le résultat sous cette forme:
```
est: 6 // est: 5 est aussi acceptable
du: 5
Pirouette: 4
cacahuete: 4
en: 4
```
