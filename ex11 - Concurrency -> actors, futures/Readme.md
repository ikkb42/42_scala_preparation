### ex11 ###
Actors, Futures

### Ce que vous savez faire ###

* du scala (sucres syntaxiques, generics, implicits, elegance...)
* du fonctionnel (immutabilite, lambdas, pattern matching...)
* de l'objet (heritage, methodes, types d'objets varies...)

### Ce que vous allez faire ###

* Decouvrir les modeles d'acteurs (et plus precisement les `Actor` de `akka`)
* Manipuler des actors
* Decouvrir les `Future`, mais ce n'est pas oblligatoire

### Ce que vous devez faire ###

* Une REPL, qui evaluera l'entree, et y appliquer la fonction factorielle
* Implementer les methodes `isNum` et `isInt`
* Implementer evidemment la fonction factorielle quelque part...
* Implementer un Actor qui reagira aux String que vous entrerez dans votre REPL (il affichera le resultat de la fonction factorielle (cf image d'exemple), ou des messages d'erreur (en cas de string invalide))
```
- une ligne vide affiche un nouveau prompt
- "exit", "quit", ou un Ctrl+d ferment votre actor, quittent votre programme en printant "Bye"
```

### Consignes ###

* Commencez par BIEN lire toute la doc
* Avant de commencer l'exo, entrainez vous, mettez en pratique ce que vous avez lu
* Pas le droit de modifier mes `trait`, vous devez tout implementer (mais vous pouez modifier le contenu de `object Console` a votre guise)
* PAS LE DROIT AUX `Thread` ! PUTAIN NON NON NON ! PAS DE JAVA ICI SVP
* Les executions de votre actor doivent etre independantes (si une factorielle est en plein calcul, vous pouvez appeler votre actor et en executer une autre oklm -> cf exemple)

![sweg](http://oi67.tinypic.com/2ladksl.jpg)
