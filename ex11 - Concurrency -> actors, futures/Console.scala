trait Strings {
  val THINK  = "Let me think..."
  val NOINT  = "Please give me an Int"
  val NONUM  = "NaN... NaNaNaNaNaNaNa BAT MAN !"
}

trait Repl {
  def read   = io.StdIn.readLine
  def prompt = print("\u001b[0mfactorial? ")
}

trait Formating extends Strings {
  implicit class Format(s:String) {
    def reform = s"${s take 50}... (${s.size} digits)"
    def isNum  = ???
    def isInt  = ???

    def red    = "\u001b[91m" + s + "\u001b[0m"
    def green  = "\u001b[92m" + s + "\u001b[0m"
    def yellow = "\u001b[93m" + s + "\u001b[0m"
  }
}

object Console extends Repl {

  def main(av:Array[String]) = ???

}
