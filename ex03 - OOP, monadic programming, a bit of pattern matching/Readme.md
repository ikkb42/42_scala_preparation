4e exo:
Comme je l’ai dit, va y avoir de la decouverte today !
On commence a accélérer le rythme :simple_smile:

```
trait Datas {
  val dic    = collection.mutable.Map[Char, Boolean]()
  val stock  = 'A' to 'Z' filter (_ % 2 == 0)
  val change = "Hello PIROUETTE CACAHUETE"
  val what   = "BoNjOuR wAlLaH !"
}
```
Je vous laisse découvrir ce qu’est un `trait` en Scala.
L’objectif du jour:
```
-> Faire un vrai programme Scala, compilable, en utilisant quelques notions importantes
  - l'objet et l'heritage
  - la mutabilité utile
  - les monades communes (ici, la monade 'Option', et les types)
  - le pattern matching

-> Creer un Object Main, avec une jolie fonction main qui devra:
  - peupler la Map 'dic' avec les Char de 'stock', en les associant à la valeur 'false'
  - updater les values de 'dic' avec les Char de 'change' en leur associant la valeur 'true' (si un Char n'existe pas en tant que clé dans 'dic', il faut l'y ajouter, et aussi lui associer la valeur 'true')
  - printer l'evaluation dans 'dic' de tous les Char de 'what' (on veut juste savoir si le Char existe dans 'dic' et connaitre sa valeur).
    -> s'il existe, vous printerez « Key -> Value »
    -> s'il n'existe pas vous printerez « Key -> ??? »

-> Consignes et recommandation
  - pas de '+=' ou de '+' pour ajouter ou updater des 'clés -> valeurs'. UTILISEZ DES METHODES FONCTIONNELLES
  - regardez en profondeur le fonctionnement de collection.mutable.Map (les methodes, les attributs etc)
  - regardez le strict minimum, les utilisations les plus communes de la monade 'Option' (et des objets qui la composent)
  - réfléchissez, mais ne vous prenez pas trop la tete, c'est juste de la découverte, pas de l'algo poussée
  - lisez, relisez tout ce que j'ai ecrit :)
```
Pour la compilation et l'execution:
```
$> scalac file.scala -d out.jar
$> scala out.jar
```
