### ex12 ###
Part 2 - SBT, c'est plein de swag

### Ce que vous allez faire ###

* Utiliser un peu mieux `sbt`
* Apprendre a utiliser le fichier de build (de maniere basique)
* Organiser vos fichiers (a la cool, strict minimum)
* Decouvrir diffrentes commandes offertes par SBT (#laDoc)

### Ce que vous devez faire ###

* Diviser le programme `Console.scala`, en 3 fichiers `Traits.scala`, `Actor.scala` et `Main.scala` (#separationOfConcerns)
* Metez vos 3 sources dans un dossier (cf `consignes`)
* Creez un fichier `build.sbt` (cf image de reference pour savoir quoi mettre dans le `build.sbt`)
* Executer le fichier en faisant d'abord un `sbt package`, puis en executant le `jar` produit

### Consignes ###

* Pas de fichier supplementaire necessaire
* Pour le placement de vos sources, respectez bien la directory structure de `sbt`
* Ignorez les warning, c'est normal pour le coup
* Je ne veux pas voir plusieurs fois le meme `import` dans differents fichiers (meme s'il n'y a pas de raison que ca se produise)

### Commentaire de l'auteur ###

`La gestion des dependances sur la JVM c'est deja pas mal foutu, mais avec SBT, ca deglingue des poneys avec plein d'arc-en-ciel`

![src](http://oi67.tinypic.com/fk1phy.jpg)
