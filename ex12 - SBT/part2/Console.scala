import System.{currentTimeMillis => now}
import akka.actor.{Actor, ActorSystem, Props}
import akka.routing.RoundRobinPool

trait Strings {
  val THINK  = "Let me think..."
  val NOINT  = "Please give me an Int"
  val NONUM  = "NaN... NaNaNaNaNaNaNa BAT MAN !"
}

trait Repl {
  def prompt = print("\u001b[0mfactorial? ")
  def read   = io.StdIn.readLine match {
    case null => "exit"
    case line => line.trim
  }
}

trait Formating extends Strings {
  implicit class Format(s:String) {
    def reform = if (s.size < 51) s else s"${s take 50}... (${s.size} digits)"
    def isNum  = ("^[0-9.]+$".r findAllIn s).nonEmpty
    def isInt  = isNum && (s.toDouble < Int.MaxValue) && (s.toDouble == s.toDouble.toInt)

    def red    = "\u001b[91m" + s + "\u001b[0m"
    def green  = "\u001b[92m" + s + "\u001b[0m"
    def yellow = "\u001b[93m" + s + "\u001b[0m"
  }
}

trait Factorial extends Formating with Repl {
  def facTimer(f: => String) = {
    val (start, res, end) = (now, f, now)
    println(s"${res.green} -> done in ${(end-start)} ms")
    prompt
  }

  def fac(x:Any)(implicit n:Int = x.toString.toDouble.toInt) =
    s"fac($n) = ${(1 to n).foldLeft(1:BigInt)(_ * _).toString.reform}"
    //s"fac($n) = ${(BigInt(1) to n).par.product.toString.reform}" //cool mais bouffe les ressources
}

class Calc extends Actor with Factorial {
  def receive = {
    case s if s.toString.isInt => println(THINK.yellow) ; prompt ; facTimer(fac(s))
    case n if n.toString.isNum => println(NOINT.red)    ; prompt
    case _                     => println(NONUM.red)    ; prompt
  }
}

object Console extends Repl {
  val system     = ActorSystem("Calculator")
  val calculator = system.actorOf(Props[Calc].withRouter(RoundRobinPool(5)), name = "calculator")

  def parse:String => Unit = _ match {
    case "exit" | "quit" => println("Bye") ; system.shutdown
    case ""              => prompt         ; repl
    case s               => calculator ! s ; repl
  }

  def repl:Unit = parse(read)

  def main(av:Array[String]) = { prompt ; repl }
}
