### ex12 ###
Part 1 - SBT, au calme

### Ce que vous allez faire ###

* Installer `sbt` (http://goo.gl/xAylJr)
* Apprendre a vous en servir

### Ce que vous devez faire (simple comme bonjour) ###

* Creer le programme `Main.scala`, qui devra afficher `Hello Sbt` en jaune
* Executer le fichier en faisant `sbt run`

### Consignes ###

* Lisez la doc, renseignez vous sur `sbt`
* Pas de fichier supplementaire necessaire

### Commentaire de l'auteur ###

`Ouais, c'est vraiment fastoche ce que je vous ai demande, mais la facilite, ben c'est cool parfois !`

![src](http://oi63.tinypic.com/fvec89.jpg)
