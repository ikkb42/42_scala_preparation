### ex12 ###
Part 3 - SBT, les plugins, au calme

### Ce que vous allez faire ###

* Mieux utiliser `sbt`
* Apprendre a appeler des plugins

### Ce que vous devez faire ###

* Utiliser la `part2` avec la lib `jline` (a la place du `readLine` pourri) -> oui il existe des libs supra cool en Java, il faut en profiter
* Ameliorer le `build.sbt` (et creer les fichiers necessaires pour l'utilisation d'un plugin sous forme de JAR)
* Compiler avec `sbt assembly` (va falloir demander de l'aide a Google), puis en executant le `jar` produit (il sera totalement compatible avec Java -> merci assembly)

### Consignes ###

* Pour le placement de vos sources, et plugins, respectez toujours la directory structure de `sbt`
* Pour l'utilisation de `jline` faites ce que vous voulez (vous verrez, jline permet plein de trucs cools. Je veux juste ne plus voir le `readLine` de Scala)
