### ex09 ###
Les types generiques

### Ce que vous savez faire ###

* de l'objet en Scala
* tester vos objets dans les REPL scala et/ou ammonite
* du pattern matching
* utiliser des lambdas (fonctions anonymes), avec diffrerentes syntaxes
* declarer et utiliser des fonctions et des methodes

### Ce que vous allez faire ###

* Changer vos habitudes de declaration de fonctions
* Creer des fonctions prenant et revoyant des types generiques
* Apprendre a avoir des fonctions en parametre
* Manipuler les parametres implicites
* Manipuler les objets implicites (on se concentrera sur les classes implicites)

```
def f(s:String):String  = s       //je ne veux pas voir ca
def f: String => String = s => s  //JE VEUX VOIR CA
```

### Ce que vous devez faire ###

* Creer une fonction `showMe` prendant une `String` et renvoyant `Unit`
  Elle devra faire un simple `println` de la String que vous lui envoyez en parametre.
* Creer une fonction `addOne` prenant un `Int` et renvoyant un `Int`
  Elle devra renvoyer votre Int d'entree en lui ajoutant 1
* Creer une fonction `doStuff` prendant un type A en entree, et qui renvoyant ce type A
  Elle renverra l'argument passe en parametre
* Creer une fonction `applySame`, elle devra prendre un parametre `x` de type A, et une fonction `f` de type A en entree et en sortie
  Elle devra appliquer la fonction `f` au parametre `x` (exemple: `applySame(42, doStuff)`)
* Creer une fonction `applyDiff` prendant un param `x` de type A, puis une fonction `f` de type A en entree, et renvoyant B en sortie
  Elle devra appliquer la fonction `f` au parametre `x` (je vous laisse chercher un exemple, impressionnez moi)

### Consignes ###

* prenez votre temps
* lisez bien la doc
* renseignez vous bien sur les types generiques
* profitez de votre weook-end de Pacques, vous avez jusqu'a Mercredi pour reflechir a tout ca
