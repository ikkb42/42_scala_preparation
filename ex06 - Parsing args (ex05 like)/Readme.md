### ex06 ###
Exercice tranquillou, vous allez utiliser vos skills de parsing en pattern matching, pour faire... `un parseur d'arguments` !!

```
Je vais mettre un bel exemple en image, mais voici les specs:

Ce parseur devra recevoir une string contenant:
- un fichier d'input  (-i | --input-file)
- un fichier d'output (-o | --output-file)
- un mode de lecture  (-d | --decompress ou -c | --compress)
```

### Consignes ###

*  Vous avez le droit d'utiliser des `var` pour stocker
*  `"Vous etes libres d'employer toutes les methodes necessaires, mais je les veux vivants"`

```
$> scalac getOpt.scala -d parse.jar
$> scala parse.jar
```

![sweg](http://oi66.tinypic.com/9hm7ie.jpg)
